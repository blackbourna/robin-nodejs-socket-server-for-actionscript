/*
 * server.js
 * 
 * RobinJS
 * 
 * Created for Robin Flash / PHP Multiuser Solution
 * https://robinflash.wordpress.com/
 * 
 * @author Andrew Blackbourn blackbourna@gmail.com
 * */
var robin = require('./robin.js').robinServer;
var express = require("express");
var app = express();
var fs = require('fs');
try {
    var env = JSON.parse(fs.readFileSync('/home/dotcloud/environment.json', 'utf-8'));
    app.use(express.static(__dirname + '/public'));
} catch (e) {
    app.use(express.static(__dirname + '/local'));
}

app.listen(8080);
robin.startServer();

