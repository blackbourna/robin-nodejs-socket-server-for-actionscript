/* server.js
 * 
 * RobinJS
 * 
 * Created for Robin Flash / PHP Multiuser Solution
 * https://robinflash.wordpress.com/
 * 
 * @author Andrew Blackbourn blackbourna@gmail.com
 * */
ROBIN_LOGS = 'logs/debuglog.txt';

function RobinServer() {
    //%^%AppName|3|1^y=5^x=20
    var net = require('net');
    var fs = require('fs');
    var port = 8081;
    try {
		var env = JSON.parse(fs.readFileSync('/home/dotcloud/environment.json', 'utf-8'));
		port = env['PORT_NODE'];
	} catch (e) {}
    var debug = false;
    var splitter = "\n________________________________________________________________________________\n";
    applicationPool = new ApplicationPool();
    function debuglog(x) {
        if (debug) console.log(x);
    }

    function writeFile(filename, writeMode, str) { // shouldn't be needed anymore, anything that goes to console.log should recorded by dotcloud (hopefully!)
	/*
        fs.open(filename, writeMode, 0666, function (err, fd) {
            fs.write(fd, str, null, undefined, function (err, written) {
                fs.close(fd);
            });
        });
	*/
    }
    //http://stackoverflow.com/questions/5223/length-of-javascript-associative-array
    Object.size = function (obj) { // .length for property maps
        var size = 0,
            key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

    function getKeys(map) {
        var keys = [];
        for (var k in map)
        keys.push(k);
        return keys;
    }

    function getArray(map) {
        var values = [];
        for (var k in map)
        values.push(map[k]);
        return values;
    }

    // can change port using cmd args
    // node filename.js -p 1234
    if (process.argv.length == 4) {
        if (process.argv[2].toUpperCase() == "-P" && !isNaN(process.argv[3])) {
            port = process.argv[3];
        }
    }

    // from http://stackoverflow.com/questions/646628/javascript-startswith
    if (typeof String.prototype.startsWith != 'function') {
        String.prototype.startsWith = function (str) {
            return this.indexOf(str) == 0;
        };
    }

    net.Socket.prototype.sendMessage = function (x) { // append null when writing to socket
		try {
			if (this.write) this.write(x + "\0");
			
        } catch (e) {
			console.log(e);
		}
    }

    var server = net.createServer(function (socket) {
        socket.on('data', function (data) {
            //console.log(" INCOMING DATA: " + data.toString());
            // Flash initially requests an XML policy file
            if (data.toString().match("<policy-file-request/>")) {
                //var policy = "<?xml version=\"1.0\"?><cross-domain-policy><allow-access-from domain=\"*\" to-ports=\"" + port + "\" /></cross-domain-policy>";
                var policy = "<?xml version=\"1.0\"?><cross-domain-policy><allow-access-from domain=\"*\" to-ports=\"*\" /></cross-domain-policy>";
                this.sendMessage(policy);
                return;
            } else if (data.toString().startsWith("%^%")) {
                var messages = data.toString().split('%^%');
                for (var m in messages) {
                    if (messages[m].length > 0) processMessage(messages[m], socket);
                }
            } else {
                socket.write("INVALID HEADER");
            }
        });
    }); // end net.createServer

    function processMessage(data, socket) { // after policy handshake, will begin to recieve messages from client
        // e.g. 
        //%^%RobinBall|10|1^y=147^x=211.95
        var message = data.split("|");
        //message[0]=RobinBall
        //message[1]=10
        //message[2]=1^y=147^x=211.95
        for (var m in message) { // remove null terminators
            message[m] = message[m].replace("\u0000", "");
        }

        var appName = message[0];
        var maxPerRoom = message[1];
        // split name/val pairs
        var variableIndex = message[2].indexOf("^");
        var fillType = message[2].substring(0, 1);
        var variableMessages = (variableIndex > -1) ? message[2].substring(variableIndex).split("^").slice(1) : [];
        if (!socket.room) { // set up new user
            var message = '';
            socket.application = applicationPool.getApplication(socket, appName, maxPerRoom, fillType);
            //socket.room = socket.application.getRoom();
            socket.variables = {};
            if (socket.room.history.length > 0) {
                message += socket.room.history + "\n";
            }
            // stuff for the history dump, not sure it's actually needed
            /*
            message += '-~^~-\n';
            message += '_u_' + socket.room.lastBroadcastIndex + '\n';
            if (socket.room.lastSender) {
                var variables = socket.room.lastSender.variables;
                for (var v in variables)
                message += v + '^' + variables[v] + '\n';
            }*/
            message += '-~^~-\n';
            if (socket.room.lastBroadcastIndex !== -1) {
                message += '_u_' + socket.room.lastBroadcastIndex + '\n';
            } else {
                message += "\n";
            }
            for (var v in socket.room.lastClientIndexToUpdateVariable) {
                message += v + '^' + socket.room.lastClientIndexToUpdateVariable[v] + '\n';
            }
            
            message += '-~^~-\n' + socket.room.buildMessage(socket, socket, -1);
            console.log('-=BEGIN INIT MESSAGE=-');
            console.log(message);
            console.log('-=END INIT MESSAGE=-');
            socket.sendMessage(message);
            socket.active = true;
            socket.on('end', function () {
                socket.room.removeSocket(socket)
            });
        } else {
            console.log('recieved: ' + data);
            var somethingChanged = false;
            for (var v in variableMessages) { // update this socket's variable hashmap
                var vSplit = variableMessages[v].split("=");
                if (vSplit[0].toLowerCase() == '_history_') {
                    socket.room.history += vSplit[1];
                } else if (vSplit[0].toLowerCase() == '_clearhistory_') {
                    socket.room.history += '';
                } else {
                    somethingChanged = true;
                    socket.variables[vSplit[0]] = vSplit[1];
                }
            }
            //console.log(socket.variables);
            if (somethingChanged) socket.room.broadcast(socket); // broadcast this user's variables
        }
    }

    function ApplicationPool() { // not quite a singleton but will do for now...
        this.applicationInstances = [];
        this.getApplication = function (socket, name, maxPerRoom, fillType) {
            if (!this.applicationInstances[name]) { // ApplicationInstance held in associate array
                this.applicationInstances[name] = new ApplicationInstance(maxPerRoom, fillType, name);
            }
            this.applicationInstances[name].addSocket(socket);
            return this.applicationInstances[name];
        }
        this.closeAllApplications = function () {
            for (var a in this.applicationInstances) {
                this.applicationInstances[a].closeApplicationInstance();
            }
            this.applicationInstances = null;
        }
    } // end ApplicationPool
    function ApplicationInstance(maxSize, fillType, name) {
        this.name = name;

        this.getRoom = function () {
            return this.rooms[this.rooms.length - 1];
        }
        this.addSocket = function (socket) {
            // 0 = don't refill slot when user leaves room
            var socketRoom = null;
            var roomAvailable = false;
            for (var r in this.rooms) {
                if (this.rooms[r].hasRoomForOneMore()) {
                    socketRoom = this.rooms[r];
                    roomAvailable = true;
                    break;
                }
            }
            console.log("Room available: " + roomAvailable);
            if (!roomAvailable) {
                this.rooms.push(new Room(fillType, maxSize));
                socketRoom = this.rooms[this.rooms.length - 1];
            }
            // add to empty socket if available, otherwise append to end!
            var addedToRoom = false;
            for (var s in socketRoom.sockets) {
				if (socketRoom.sockets[s].clientID == '') {
					socket.clientIndex = socketRoom.sockets[s].clientIndex
					socketRoom.sockets[s] = socket;
					addedToRoom = true;
					console.log('Added to open slot');
					break;
				}
			}
			if (!addedToRoom) {
				socketRoom.sockets.push(socket);
				socket.clientIndex = socketRoom.sockets.length - 1;
			}
			
            socket.clientID = socketRoom.getNewClientID();
            socket.room = socketRoom;
            debuglog('Added ' + socket.clientID + ' to ' + ((roomAvailable) ? ' existing' : ' newly created') + ' room');
        }
        if (!this.rooms) {
            this.rooms = [new Room(fillType, maxSize)]; // ApplicationInstance contains an array of Room objects
        }
        this.closeApplicationInstance = function () {
            for (var r in this.rooms) {
                this.rooms[r].closeRoom();
            }
            this.rooms = null;
        }
    } // end ApplicationInstance

    function Room(fillType, maxSize) {
        var self = this;
        if (!this.sockets) {
            this.fillType = fillType;
            this.maxSize = maxSize;
            this.sockets = [];
            this.clientIDs = [];
            this.lastBroadcastIndex = -1; // last client that sent data
            this.history = "";
            this.lastMessage = "";
            this.currentVariables = {};
            this.lastClientIndexToUpdateVariable = {};
            this.lastSender = null;
        }
        this.closeRoom = function () {
            for (var s in this.sockets) {
                this.sockets[s] = null;
            }
            this.sockets = null;
        }
        
        this.buildMessage = function(sender, recievingSocket, idx) {
            if (!recievingSocket
                || recievingSocket == sender && idx != -1
                || recievingSocket.clientID == '') return '';
            console.log('BUILD MESSAGE CALLED');
            var message = "";
            var relativeIndex = idx;
            //*
            // original
            if (idx > -1) {
                var relativeIndex = (sender.clientIndex > idx) ? sender.clientIndex - 1 : sender.clientIndex;
            }
            relativeIndex = relativeIndex % self.maxSize;
            message += relativeIndex + "\n"; // socket relative to receiver's index
            
            if (idx == -1) {
                for (var s in this.sockets) {
                    console.log('socket')
                    for (var v in this.sockets[s].variables) {
                        console.log('variable');
                        var val = sender.variables[v];
                        if (!val && (val !== '') && (val !== 0)) {
                            console.log('added var');
                            sender.variables[v] = '';
                        }
                    }
                }
            }
            
            if (getKeys(sender.variables).length) {
                message += getKeys(sender.variables).join("|") + "\n";
            }
            message += "_u_^" + self.getOtherClientIDs(recievingSocket).join('|') + '\n';
            // end original
            //*/
            /*
             // from March 6 email:
            if (idx > -1) {
                var relativeIndex = (sender.clientIndex > idx) ? sender.clientIndex - 1 : sender.clientIndex;
            }            
            if (idx == -1) {

                for (var s in this.sockets) {
                    console.log('socket')
                    for (var v in this.sockets[s].variables) {
                        console.log('variable');
                        var val = sender.variables[v];
                        if (!val && (val !== '') && (val !== 0)) {
                            console.log('added var');
                            sender.variables[v] = '';
                        }
                    }
                }
            }
            if (getKeys(sender.variables).length) {

                message += getKeys(sender.variables).join("|") + "\n";
            }            
            message += "_u_^" + self.getOtherClientIDs(recievingSocket).join('|') + '\n';
            //*/
            // end March 6 email
            /* // From march 6 FB message 
            if (idx > -1) {
                var relativeIndex = (sender.clientIndex > idx) ? sender.clientIndex - 1 : sender.clientIndex;
            }
            relativeIndex = relativeIndex % self.maxSize;
            message += relativeIndex + "\n"; // socket relative to receiver's index
            message += "_u_^" + self.getOtherClientIDs(recievingSocket).join('|') + '\n';
            if (idx == -1) {
                for (var s in this.sockets) {
                    console.log('socket')
                    for (var v in this.sockets[s].variables) {
                        console.log('variable');
                        var val = sender.variables[v];
                        if (!val && (val !== '') && (val !== 0)) {
                            console.log('added var');
                            sender.variables[v] = '';
                        }
                    }11
                }
            }
            if (getKeys(sender.variables).length) {
                message += getKeys(sender.variables).join("|") + "\n";
            }
            // end march 6 FB message
            //*/
            for (var v in sender.variables) { // loop through sender's variables
                if (v.toLowerCase == '_history_' || v.toLowerCase == '_clearhistory_') continue;
                // prepare message
                message += v + "^";
                var others = self.getOthersInRoom(recievingSocket);
                var otherVars = [];
                for (var o in others) {
                    if (others[o]) {
                        if (others[o].variables[v]) {
                            otherVars.push(others[o].variables[v]);
                        } else {
                            otherVars.push('');
                        }
                    }
                }
                message += otherVars.join('|') + "\n";
            }
            return message;
        }
        
        this.broadcast = function (sender) {
            console.log('BROADCAST CALLED');
			console.log(this.countNonEmptySockets() + " users in the room");
            this.lastBroadcastIndex = sender.clientIndex;
            debuglog('sender is: ' + sender.clientID);
            debuglog("sender's variables: ");

            for (var v in sender.variables) { // loop through sender's variables
                // used for _history messages
                if (this.currentVariables[v] != sender.variables[v]) {
                    this.lastClientIndexToUpdateVariable[v] = sender.clientIndex;
                }
                this.currentVariables[v] = sender.variables[v];
                this.lastSender = sender;
            }
            var sentCount = 0;
            // countNonEmptySockets counts based on whether clientID.length > 0, but clientID is empty when a socket is removed
            /*if (this.countNonEmptySockets() == 1) { // only 1 socket in the room!
                return;
                var message = sender.clientIndex + '\n';
                message += getKeys(sender.variables).join("|") + "\n";
                message += "_u_^" + sender.clientID + '\n';
                for (var v in sender.variables) {
                    if (v.toLowerCase == '_history_' || v.toLowerCase == '_clearhistory_') continue;
                    // prepare message
                    message += v + "^" + sender.variables[v] + '\n';
                }
                this.lastMessage = message;
                console.log("Only 1 user in the room!");
                //sender.sendMessage(message);
                for (var s = 0; s < this.sockets.length; s++) {
                    var lastSocket = this.sockets[s];
                    lastSocket.room.broadcast(lastSocket);
                }
                return;
            }*/
            for (var s = 0; s < this.sockets.length; s++) { // loop through non-sender sockets
                var recievingSocket = this.sockets[s];
                var message = this.buildMessage(sender, recievingSocket, s);
                if (!message) continue;
                this.lastMessage = message;
                //debuglog('SENDING TO: ' + recievingSocket.clientID);
                //debuglog(message + splitter);
                console.log(message + splitter);
                recievingSocket.sendMessage(message);
                this.lastMessage = message;
                sentCount++;
            }
            debuglog('sent ' + sentCount + ' messages');
        }
        this.users = function () {
            var users = 'List of users: ';
            for (var s in this.sockets) {
                if (this.sockets[s] == null) continue;
                users += this.sockets[s].clientID + ' ';
            }
            return users;
        }
        this.removeSocket = function (socket) {
            for (var i = 0; i < this.sockets.length; i++) {
                if (this.sockets[i] == socket) {
                    var s = this.sockets[i];
                    if (i == this.sockets.length - 1) {
                        for (var v in s.variables) {
                            s.variables[v] = '';
                        }
                        if (this.lastBroadcastIndex == s.clientIndex) this.lastBroadcastIndex = -1;
                        // Don't strip off the trailing socket!
                        //this.sockets.splice(i, 1);
                    } else {
                        if (this.lastBroadcastIndex == s.clientIndex) this.lastBroadcastIndex = -1;
                        for (var v in s.variables) {
                            s.variables[v] = '';
                        }
                    }
                    s.clientID = '';
                    s.room.broadcast(s);
                    s.active = false;
                    break;
                }
            }
            console.log("socket removed!");
            
            if (this.sockets.length == 0) this.history = '';
        }
        this.hasRoomForOneMore = function () {
            if (this.fillType == 1) {
                return this.countNonEmptySockets() < this.maxSize;
            } else {
                return this.sockets.length < this.maxSize;
            }
        }
        this.countNonEmptySockets = function () {
            var count = 0;
            for (var s in this.sockets) {
				console.log('id: '+this.sockets[s].clientID);
                if (this.sockets[s].active) count++;
            }
            console.log("There are " + count + " sockets current in this room. MaxSize = " + this.maxSize);
            return count;
        }
        this.getOthersInRoom = function (socket) {
            var others = [];
            for (var s in this.sockets) {
                if (socket != this.sockets[s] && this.sockets[s] != null && this.sockets[s] !== undefined) others.push(this.sockets[s]);
            }
            return others;
        }
        this.getOtherClientIDs = function (socket) {
            var others = this.getOthersInRoom(socket);
            var otherIDs = [];
            for (var o in others)
            otherIDs.push(others[o].clientID);
            return otherIDs;
        }
        this.getNewClientID = function () {
            var id;
            while (true) {
                id = Math.floor(Math.random() * 1000);
                if (this.clientIDs.indexOf(id) == -1) break;
            }
            return id;
        }
    } // end Room
    this.startServer = function () {
        server.listen(port);
        console.log("Robin server started on port " + port + "...");
    }
    this.stopServer = function () {
        applicationPool.closeAllApplications();
        server.close();
    }
    this.setPort = function (customPort) {
        this.port = customPort;
    }
}
exports.robinServer = new RobinServer();
